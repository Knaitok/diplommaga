﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace HighContrastMagnifier.Image
{
    public interface IStrategy
    {
        void SetImage(WriteableBitmap image);
        WriteableBitmap GetImage();
        void GreyImage();
        void ColorImage();
        void BilinearInterpolation();
        void BicubicInterpolation();
        void ProcessImage();

    }
}
