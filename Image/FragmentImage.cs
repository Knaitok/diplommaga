﻿using HighContrastMagnifier.Models;
using HighContrastMagnifier.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace HighContrastMagnifier.Image
{
    public class FragmentImage : IStrategy
    {
        private WriteableBitmap Image;
        private WriteableBitmap FinalImage;
        private WriteableBitmap ProcessingImage;
        private double KoefScale;
        public void BicubicInterpolation()
        {
            if (Image != null)
            {
                FinalImage = Interpolations.BicubicInterpolation(ProcessingImage, KoefScale);
            }
        }

        public void BilinearInterpolation()
        {
            if (Image != null)
            {
                FinalImage = Interpolations.BilinearInterpolation(ProcessingImage,KoefScale);
            }
        }

        public void ColorImage()
        {
            if (Image != null)
            {
                ProcessingImage = Algorithms.ColorImage(Image);
            }
        }

        public WriteableBitmap GetImage()
        {
            return FinalImage;
        }

        public void GreyImage()
        {
            if (Image != null)
            {
                ProcessingImage = Algorithms.HightContrastGrey(Image);
            }
        }

        public void ProcessImage()
        {
            switch (Settings.ImageColor)
            {
                case TypeColor.Color:
                    ColorImage();
                    break;
                case TypeColor.Grey:
                    GreyImage();
                    break;
            }
            switch (Settings.Interpolation)
            {
                case InterpolationType.bicubic:
                    BicubicInterpolation();
                    break;
                case InterpolationType.bilinear:
                    BilinearInterpolation();
                    break;
            }
        }

        public void SetImage(WriteableBitmap image)
        {
            Image = image;
            KoefScale = Image.PixelWidth > Image.PixelHeight
                    ? ParamImage.SizeVirtualFragmentImage.Width / Image.PixelWidth
                    : ParamImage.SizeVirtualFragmentImage.Height / Image.PixelHeight;
        }
    }
}
