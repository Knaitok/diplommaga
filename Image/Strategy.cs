﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace HighContrastMagnifier.Image
{
    public class Strategy
    {
        private IStrategy _strategy;
        public Strategy(IStrategy strategy)
        {
            this._strategy = strategy;
        }
        public void InitImage(WriteableBitmap image)
        {
            _strategy.SetImage(image);
        }
        public void BicubicInterpolation()
        {
            _strategy.BicubicInterpolation();
        }

        public void BilinearInterpolation()
        {
            _strategy.BilinearInterpolation();
        }

        public void ColorImage()
        {
            _strategy.ColorImage();
        }

        public void GreyImage()
        {
            _strategy.GreyImage();
        }
        public WriteableBitmap GetImage()
        {
            return _strategy.GetImage();
        }
        public void ProcessImage()
        {
            _strategy.ProcessImage();
        }
    }
}
