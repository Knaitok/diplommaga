﻿using HighContrastMagnifier.Models;
using HighContrastMagnifier.Services;
using HighContrastMagnifier.Services.Interfaces;
using HighContrastMagnifier.Views;
using Prism.Ioc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace HighContrastMagnifier
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App
    {
        protected override Window CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<IImageProcessingService, ImageProcessingService>();
            containerRegistry.RegisterSingleton<Settings>();
        }
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            Container.Resolve<Settings>();
        }
    }

}
