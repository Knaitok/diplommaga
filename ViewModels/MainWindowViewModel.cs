﻿using HighContrastMagnifier.Models;
using HighContrastMagnifier.Services;
using HighContrastMagnifier.Services.Interfaces;
using HighContrastMagnifier.Tools;
using Microsoft.Win32;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace HighContrastMagnifier.ViewModels
{
    public class MainWindowViewModel:BindableBase
    {
        public DelegateCommand OpenImageCommand { get; set; }
        public DelegateCommand ProcessCommand { get; set; }
        public DelegateCommand SaveFragmentCommand { get; set; }

        private readonly IImageProcessingService service;

        public MainWindowViewModel(IImageProcessingService service)
        {
            OpenImageCommand = new DelegateCommand(OpenImage);
            ProcessCommand = new DelegateCommand(Prcess);
            SaveFragmentCommand = new DelegateCommand(SaveFragment);
            this.service = service;
            service.ImageSource += Service_ImageSource;
            service.ImageFragment += Service_ImageFragment;
            Type = new List<string> { "В отенках серого", "Цветное" };
            RGBType = new List<string> { "R=0.299, G=0.587, B=0.114", "R=0.2126, G=0.7152, B=0.0722" };
            Interpolation = new List<string> { "Билинейная", "Бикубическая" };
            SelectedType = 0;
            SelectedRGB = 0;
            SelectedInterpolation = 0;
            VisibilityData = true;
        }

        private void SaveFragment()
        {
            service.SaveImage();
        }

        private void Service_ImageFragment(WriteableBitmap fragment) => ResultImage = fragment;

        private void Service_ImageSource(WriteableBitmap source) => SourceImage = source;

        private void Prcess()
        {
            service.FragmentTransformation(CurrentArea);
        }

        private void OpenImage()
        {
            service.OpenImage();
        }
        #region Property
        private WriteableBitmap _sourceImage;
        public WriteableBitmap SourceImage
        {
            get => _sourceImage;
            set => SetProperty(ref _sourceImage, value);
        }
        private WriteableBitmap _resultImage;
        public WriteableBitmap ResultImage
        {
            get => _resultImage;
            set => SetProperty(ref _resultImage, value);
        }
        private List<string> _type;
        public List<string> Type
        {
            get => _type;
            set => SetProperty(ref _type, value);
        }
        private int _selectedType;
        public int SelectedType
        {
            get => _selectedType;
            set
            {
                SetProperty(ref _selectedType, value);
                Settings.ImageColor = (TypeColor)value;
            }
        }
        private List<string> _interpolation;
        public List<string> Interpolation
        {
            get => _interpolation;
            set => SetProperty(ref _interpolation, value);
        }
        private int _selectedInterpolation;
        public int SelectedInterpolation
        {
            get => _selectedInterpolation;
            set
            {
                SetProperty(ref _selectedInterpolation, value);
                Settings.Interpolation = (InterpolationType)value;
            }
        }
        private List<string> _rgbType;
        public List<string> RGBType
        {
            get => _rgbType;
            set => SetProperty(ref _rgbType, value);
        }
        private int _selectedRGB;
        public int SelectedRGB
        {
            get => _selectedRGB;
            set
            {
                SetProperty(ref _selectedRGB, value);
                Settings.RGBImage = (TypeRGB)value;
            }
        }
        private bool _visibilityData;
        public bool VisibilityData
        {
            get => _visibilityData;
            set => SetProperty(ref _visibilityData, value);
        }
        private Size _sizeAreaSourceImage;
        public Size SizeAreaSourceImage
        {
            get => _sizeAreaSourceImage;
            set {
                SetProperty(ref _sizeAreaSourceImage, value);
                ParamImage.SizeVirtualSourceImage = value;
            }
        }
        public Size SizeAreaFragmentImage
        {
            get => _sizeAreaFragmentImage;
            set
            {
                SetProperty(ref _sizeAreaFragmentImage, value);
                ParamImage.SizeVirtualFragmentImage = value;
            }
        }
        private double _mathExpectation;
        public double MathExpectation
        {
            get =>_mathExpectation;
            set
            {
                SetProperty(ref _mathExpectation, value);
                ParamAlgorihms.MathExpectation = value;
            }
        }
        private double _deviation;
        public double Deviation
        {
            get => _deviation;
            set {
                SetProperty(ref _deviation, value);
                ParamAlgorihms.Deviation = value;
            }
        }
        private AreaImage _currentArea;
        private Size _sizeAreaFragmentImage;

        public AreaImage CurrentArea
        {
            get => _currentArea;
            set
            {
                SetProperty(ref _currentArea, value);
                service.FragmentTransformation(value);
            }
        }
        #endregion
    }
}
