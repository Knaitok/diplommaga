﻿using HighContrastMagnifier.Models;
using HighContrastMagnifier.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HighContrastMagnifier.Views
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //private Point startPoint;
        //private Point endPoint;
        //private Rectangle rect;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Image_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            (this.DataContext as MainWindowViewModel).SizeAreaSourceImage = e.NewSize;
        }

        private Point startPoint;
        private Point endPoint;
        private Rectangle rect;

        private void Canvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            canvas.Children.Remove(rect);

            startPoint = e.GetPosition(image);

            rect = new Rectangle
            {
                Stroke = Brushes.White,
                StrokeThickness = 1,
                StrokeDashArray = new DoubleCollection() { 1,2 }
            };
            Canvas.SetLeft(rect, startPoint.X);
            Canvas.SetTop(rect, startPoint.Y);
            canvas.Children.Add(rect);
        }

        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Released || rect == null)
                return;

            endPoint = e.GetPosition(image);

            var x = Math.Min(endPoint.X, startPoint.X);
            var y = Math.Min(endPoint.Y, startPoint.Y);

            var w = Math.Max(endPoint.X, startPoint.X) - x;
            var h = Math.Max(endPoint.Y, startPoint.Y) - y;

            rect.Width = w;
            rect.Height = h;
            if (image.ActualHeight < endPoint.Y)
            {
                rect.Height = rect.Height-(endPoint.Y-image.ActualHeight);
                endPoint.Y = rect.Width;
            }
            

            Canvas.SetLeft(rect, x);
            Canvas.SetTop(rect, y);
        }

        private void Canvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            AreaImage area = new AreaImage
            {
                StartPoint = startPoint,
                EndPoint = endPoint,
                Width = (int)rect.Width,
                Height = (int)rect.Height
            };
            (this.DataContext as MainWindowViewModel).CurrentArea = area;
        }

        private void Grid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            (this.DataContext as MainWindowViewModel).SizeAreaFragmentImage = e.NewSize;
        }
    }
}
