﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace HighContrastMagnifier.Models
{
    public class AreaImage
    {
        public Point StartPoint { get; set; }
        public Point EndPoint { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
}
