﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace HighContrastMagnifier.Models
{
    public static class ParamImage
    {
        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;
        private static string _widthImage;
        public static string WidthImage
        {
            get { return _widthImage; }
            set { 
                _widthImage = value;
                StaticPropertyChanged?.Invoke(null, new PropertyChangedEventArgs(nameof(WidthImage)));
            }
        }
        private static string _heightImage;
        public static string HeightImage 
        {
            get { return _heightImage; }
            set 
            {
                _heightImage = value;
                StaticPropertyChanged?.Invoke(null, new PropertyChangedEventArgs(nameof(HeightImage)));
            }
        }
        private static string _widthFragment;
        public static string WidthFragment 
        {
            get { return _widthFragment; }
            set 
            {
                _widthFragment = value;
                StaticPropertyChanged?.Invoke(null, new PropertyChangedEventArgs(nameof(WidthFragment)));
            }
        }
        private static string _heightFragment;
        public static string HeightFragment 
        {
            get { return _heightFragment; }
            set
            {
                _heightFragment = value;
                StaticPropertyChanged?.Invoke(null, new PropertyChangedEventArgs(nameof(HeightFragment)));
            }
        }
        private static string _timeLoadImage;
        public static string TimeLoadImage 
        {
            get { return _timeLoadImage; }
            set
            {
                _timeLoadImage = value;
                StaticPropertyChanged?.Invoke(null, new PropertyChangedEventArgs(nameof(TimeLoadImage)));
            }
        }
        private static string _timeProcess;
        public static string TimeProcess 
        {
            get { return _timeProcess; }
            set
            {
                _timeProcess = value;
                StaticPropertyChanged?.Invoke(null, new PropertyChangedEventArgs(nameof(TimeProcess)));
            }
        }
        public static Size SizeVirtualSourceImage{get; set; }
        public static Size SizeVirtualFragmentImage { get; set; }

    }
}
