﻿using HighContrastMagnifier.Image;
using HighContrastMagnifier.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HighContrastMagnifier.Models
{
    public enum InterpolationType
    {
        bilinear,
        bicubic
    }
    public enum TypeImage
    {
        Source,
        Fragment
    }
    public enum TypeColor
    {
        [Description("В оттенках серого")]
        Grey,
        [Description("Цветное")]
        Color
    }
    public enum TypeRGB
    {
        oldRGB,
        newRGB
    }
    public class Settings
    {
        private static IImageProcessingService service;
        public static Strategy StrategySource { get; set; } 
        public static Strategy StrategyFragment { get; set; } 
        public Settings(IImageProcessingService serviceImage)
        {
            service = serviceImage;
        }

        private static TypeColor _imageColor;
        public static TypeColor ImageColor 
        {
            get { return _imageColor; }
            set
            {
                _imageColor = value;
                if(service != null)
                {
                    StrategySource.ProcessImage();
                    StrategyFragment.ProcessImage();
                    service.UpdateImage();
                }
            }
        }
        private static TypeRGB _rgbImage;
        public static TypeRGB RGBImage
        {
            get { return _rgbImage; }
            set
            {
                _rgbImage = value;
                switch (value)
                {
                    case TypeRGB.oldRGB:
                        ParamAlgorihms.RGBColor = new ParamAlgorihms.RGB(0.299,0.587,0.114);
                        break;
                    case TypeRGB.newRGB:
                        ParamAlgorihms.RGBColor = new ParamAlgorihms.RGB(0.2126,0.7152,0.0722);
                        break;
                }
                if(service != null)
                {
                    ImageColor = ImageColor;
                }
            }
        }
        private static InterpolationType _interpolation;
        public static InterpolationType Interpolation
        {
            get { return _interpolation; }
            set
            {
                _interpolation = value;
                if (service != null)
                {
                    switch (value)
                    {
                        case InterpolationType.bilinear:
                            StrategySource.BilinearInterpolation();
                            StrategyFragment.BilinearInterpolation();
                            service.UpdateImage();
                            break;
                        case InterpolationType.bicubic:
                            StrategySource.BicubicInterpolation();
                            StrategyFragment.BicubicInterpolation();
                            service.UpdateImage();
                            break;
                    }
                }
            }
        }
    }
}
