﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HighContrastMagnifier.Models
{
    public static class ParamAlgorihms
    {
        public struct RGB
        {
            public RGB(double R, double G, double B)
            {
                this.R = R;
                this.G = G;
                this.B = B;
            }
            public double R;
            public double G;
            public double B;
        }
        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;
        private static double _y;
        public static double Y 
        {
            get { return _y; }
            set
            {
                _y = value;
                
            }
        }
        private static double _y2;
        public static double Y2 
        {
            get { return _y2; }
            set
            {
                _y2 = value;
                
            }
        }
        private static double _y2_sr;
        public static double Y2_sr 
        {
            get { return _y2_sr; }
            set
            {
                _y2_sr = value;
                
            }
        }
        private static double _y_sr;
        public static double Y_sr 
        {
            get { return _y_sr; }
            set
            {
                _y_sr = value;
               
            }
        }
        private static double _q;
        public static double q 
        {
            get { return _q; }
            set
            {
                _q = value;
                StaticPropertyChanged?.Invoke(null, new PropertyChangedEventArgs(nameof(q)));
            }
        }
        private static double _gy;
        public static double Gy 
        {
            get { return _gy; }
            set
            {
                _gy = value;
                
            }
        }
        private static double _gz;
        public static double Gz 
        {
            get { return _gz; }
            set
            {
                _gz = value;
                
            }
        }
        private static double _k;
        public static double k
        {
            get => _k;
            set
            {
                _k = value;
                StaticPropertyChanged?.Invoke(null, new PropertyChangedEventArgs(nameof(k)));
            }
        }
        public static double MathExpectation { get; set; }
        public static double Deviation { get; set; }
        public static RGB RGBColor { get; set; }
    }
}
