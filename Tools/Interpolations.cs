﻿using HighContrastMagnifier.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace HighContrastMagnifier.Tools
{
    public static class Interpolations
    {
        public static WriteableBitmap BilinearInterpolation(WriteableBitmap source, double scale)
        {
            int stride = source.PixelWidth * (source.Format.BitsPerPixel / 8);
            byte[] pixels = new byte[source.PixelHeight * stride];
            source.CopyPixels(new Int32Rect(0, 0, source.PixelWidth, source.PixelHeight), pixels, stride, 0);
            WriteableBitmap Image = BitmapFactory.New(source.PixelWidth, source.PixelHeight).FromByteArray(pixels);
            Image = Image.Resize((int)(source.PixelWidth*scale), (int)(source.PixelHeight*scale), WriteableBitmapExtensions.Interpolation.Bilinear);

            return Image;
        }
        public static WriteableBitmap BicubicInterpolation(WriteableBitmap source, double scale)
        {
            var map = GetPixelMap(source);
            var image = Bicubic(map, scale);
            WriteableBitmap Image = BitmapFactory.New(image.GetLength(0), image.GetLength(1)).FromByteArray(getPixelImage(image));
            return Image;
        }
        private struct Pixel
        {
            public Pixel(int A, int R, int G, int B)
            {
                this.A = A;
                this.B = B;
                this.R = R;
                this.G = G;
            }
            public int A;
            public int R;
            public int G;
            public int B;
        }
        private static Pixel[,] GetPixelMap(WriteableBitmap wb)
        {

            var width = wb.PixelWidth;
            var height = wb.PixelHeight;
            var bpp = wb.Format.BitsPerPixel;
            var dpiX = wb.DpiX;
            var dpiY = wb.DpiY;

            Pixel[,] source = new Pixel[wb.PixelWidth,wb.PixelHeight];

            var stride = wb.PixelWidth * ((bpp + 7) / 8);

            var data = new byte[width * height * 4];
            wb.CopyPixels(data, stride, 0);

            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    source[x, y] = new Pixel
                    {
                        R = data[(y * width + x) * 4 + 0],
                        G = data[(y * width + x) * 4 + 1],
                        B = data[(y * width + x) * 4 + 2],
                        A = data[(y * width + x) * 4 + 3]
                    };
                }
            }
            return source;
        }
        private static int getBicPixelChannel(Pixel[,]image, int x, int y, int k)
        {
            if (x >= 0 && y >= 0)
            {
                switch (k)
                {
                    case 0:
                        if (x < image.GetLength(0) && y < image.GetLength(1))
                            return image[x, y].R;
                        else return 0;
                    case 1:
                        if (x < image.GetLength(0) && y < image.GetLength(1))
                            return image[x, y].G;
                        else return 0;
                    case 2:
                        if (x < image.GetLength(0) && y < image.GetLength(1))
                            return image[x, y].B;
                        else return 0;
                }
            }
            return 0;
           
        }
        private static byte[] getPixelImage(Pixel[,] image)
        {
            int k = 0;
            byte[] pixels = new byte[image.GetLength(0) * image.GetLength(1) * 4];
            for (int i = 0; i < image.GetLength(1); i++)
            {
                for (int j = 0; j < image.GetLength(0); j++)
                {
                    pixels[k] = (byte)image[j, i].R;
                    pixels[k+1] = (byte)image[j, i].G;
                    pixels[k+2] = (byte)image[j, i].B;
                    pixels[k + 3] = (byte)image[j, i].A;
                    k += 4;
                }
            }
            return pixels;
        }
        private static Pixel[,] Bicubic(Pixel[,] image,double rate)
        {
            int new_w = (int)(image.GetLength(0) * rate);
            int new_h = (int)(image.GetLength(1) * rate);

            Pixel[,] new_img = new Pixel[new_w, new_h];

            double x_rate = (double)image.GetLength(0) / new_img.GetLength(0);
            double y_rate = (double)image.GetLength(1) / new_img.GetLength(1);

            double [] C = new double[5];
            for (int hi = 0; hi < new_img.GetLength(1); hi++)
            {
                for (int wi = 0; wi < new_img.GetLength(0); wi++)
                {
                    int x_int = (int)(wi * x_rate);
                    int y_int = (int)(hi * y_rate);

                    double dx = x_rate * wi - x_int;
                    double dy = y_rate * hi - y_int;
                    for (int k = 0; k < 3; ++k)
                    {
                        for (int jj = 0; jj < 4; ++jj)
                        {
                            var o_y = y_int - 1 + jj;
                            var a0 = getBicPixelChannel(image, x_int, o_y, k);
                            var d0 = getBicPixelChannel(image, x_int - 1, o_y, k) - a0;
                            var d2 = getBicPixelChannel(image, x_int + 1, o_y, k) - a0;
                            var d3 = getBicPixelChannel(image, x_int + 2, o_y, k) - a0;

                            var a1 = -1.0 / 3 * d0 + d2 - 1.0 / 6 * d3;
                            var a2 = 1.0 / 2 * d0 + 1.0 / 2 * d2;
                            var a3 = -1.0 / 6 * d0 - 1.0 / 2 * d2 + 1.0 / 6 * d3;
                            C[jj] = a0 + a1 * dx + a2 * dx * dx + a3 * dx * dx * dx;
                            d0 = (int)(C[0] - C[1]);
                            d2 = (int)(C[2] - C[1]);
                            d3 = (int)(C[3] - C[1]);
                            a0 = (int)C[1];
                            a1 = -1.0 / 3 * d0 + d2 - 1.0 / 6 * d3;
                            a2 = 1.0 / 2 * d0 + 1.0 / 2 * d2;
                            a3 = -1.0 / 6 * d0 - 1.0 / 2 * d2 + 1.0 / 6 * d3;
                            var pixel = (int)(a0 + a1 * dy + a2 * dy * dy + a3 * dy * dy * dy);
                            if (pixel > 255) pixel = 255;
                            if (pixel < 0) pixel = 0;
                            switch (k)
                            {
                                case 0:
                                    new_img[wi, hi].R = pixel;
                                    break;
                                case 1:
                                    new_img[wi, hi].G = pixel;
                                    break;
                                case 2:
                                    new_img[wi, hi].B = pixel;
                                    new_img[wi, hi].A = 255;
                                    break;
                            }
                            
                        }
                    }
                }
            }
            return new_img;
        }
    }
}
