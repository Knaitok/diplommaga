﻿using HighContrastMagnifier.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace HighContrastMagnifier.Tools
{
    public static class Algorithms
    {

        public static WriteableBitmap Grey(WriteableBitmap source)
        {
            int stride = (int)source.PixelWidth * (source.Format.BitsPerPixel / 8);
            byte[] pixels = new byte[(int)source.PixelHeight * stride];
            source.CopyPixels(pixels, stride, 0);
            WriteableBitmap Image = source.Clone();
            Image.WritePixels(new Int32Rect(0, 0, source.PixelWidth, source.PixelHeight), pixels, stride, 0);
            try
            {
                Image.Lock();
                int n = 0;
                for (int row = 0; row < Image.PixelHeight; row++)
                {
                    for (int column = 0; column < Image.PixelWidth; column++)
                    {
                        unsafe
                        {
                            IntPtr pBackBuffer = Image.BackBuffer;
                            int color = (int)(pixels[n] * ParamAlgorihms.RGBColor.R + pixels[n + 1] * ParamAlgorihms.RGBColor.G + pixels[n+2] * ParamAlgorihms.RGBColor.B);
                            if(color > 255)
                            {
                                color = 255;
                            }
                            n += 4;
                            //Находим адрес пикселя
                            pBackBuffer += row * Image.BackBufferStride;
                            pBackBuffer += column*4;

                            // Создаем новый цвет
                            int color_data = 255 << 24;
                            color_data |= color << 16; // R
                            color_data |= color << 8;   // G
                            color_data |= color << 0;   // B

                            // Изменяем цвет пикселя
                            *((int*)pBackBuffer) = color_data;
                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show("Ошибка в преобразовании изображения в оттенки серого");
            }
            finally
            {
                Image.Unlock();
            }
            return Image;
        }
        public static WriteableBitmap HightContrastGrey ( WriteableBitmap source)
        {
            ParamAlgorihms.Y = default;
            ParamAlgorihms.Y2 = default;
            //для черно-белого
            ParamAlgorihms.Gz = ParamAlgorihms.Deviation;
            int stride = source.PixelWidth * (source.Format.BitsPerPixel / 8);
            byte[] pixels = new byte[source.PixelHeight * stride];
            source.CopyPixels(pixels, stride, 0);
            WriteableBitmap Image = source.Clone();
            for (int i = 0; i < pixels.Length; i += 4)
            {
                var Y_tek = ParamAlgorihms.RGBColor.R * pixels[i+2] + ParamAlgorihms.RGBColor.G * pixels[i+1] + ParamAlgorihms.RGBColor.B * pixels[i];
                ParamAlgorihms.Y += Y_tek;
                ParamAlgorihms.Y2 += Math.Pow(Y_tek, 2);
            }
            ParamAlgorihms.Y2_sr = ParamAlgorihms.Y2 / (Image.Width * Image.Height);//квадрат средней яркости
            ParamAlgorihms.Y = ParamAlgorihms.Y / (Image.Width * Image.Height);//Средняя яркость 
            ParamAlgorihms.q = ParamAlgorihms.MathExpectation / ParamAlgorihms.Y;
            ParamAlgorihms.Gy = Math.Sqrt(ParamAlgorihms.Y2_sr - Math.Pow(ParamAlgorihms.Y, 2));
            ParamAlgorihms.k = ((1 / ParamAlgorihms.q) * (ParamAlgorihms.Gz / ParamAlgorihms.Gy)) - 1;
            try
            {
                Image.Lock();
                int n = 0;
                for (int row = 0; row < Image.PixelHeight; row++)
                {
                    for (int column = 0; column < Image.PixelWidth; column++)
                    {
                        unsafe
                        {
                            IntPtr pBackBuffer = Image.BackBuffer;
                            var color = Convert.ToInt32(ParamAlgorihms.q * (pixels[n] + ParamAlgorihms.k * (pixels[n] - ParamAlgorihms.Y)));
                            if (color > 255)
                            {
                                color = 255;
                            }else if (color < 0)
                            {
                                color = 0;
                            }
                            n += 4;
                            //Находим адрес пикселя
                            pBackBuffer += row * Image.BackBufferStride;
                            pBackBuffer += column * 4;

                            // Создаем новый цвет
                            int color_data = 255 << 24;
                            color_data |= color << 16; // R
                            color_data |= color << 8;   // G
                            color_data |= color << 0;   // B

                            // Изменяем цвет пикселя
                            *((int*)pBackBuffer) = color_data;
                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show("Ошибка в преобразовании фрагмента");
            }
            finally
            {
                Image.Unlock();
            }
            return Image;
        }
        public static WriteableBitmap ColorImage(WriteableBitmap source)
        {
            ParamAlgorihms.Y = default;
            ParamAlgorihms.Y2 = default;
            ParamAlgorihms.Gz = ParamAlgorihms.Deviation;
            int stride = source.PixelWidth * (source.Format.BitsPerPixel / 8);
            byte[] pixels = new byte[source.PixelHeight * stride];
            source.CopyPixels(pixels, stride, 0);
            WriteableBitmap Image = source.Clone();
            for (int i = 0; i < pixels.Length; i += 4)
            {
                var Y_tek = ParamAlgorihms.RGBColor.B * pixels[i + 2] + ParamAlgorihms.RGBColor.G * pixels[i + 1] + ParamAlgorihms.RGBColor.R * pixels[i];
                ParamAlgorihms.Y += Y_tek;
                ParamAlgorihms.Y2 += Math.Pow(Y_tek, 2);
            }
            ParamAlgorihms.Y2_sr = ParamAlgorihms.Y2 / (Image.Width * Image.Height);//квадрат средней яркости
            ParamAlgorihms.Y = ParamAlgorihms.Y / (Image.Width * Image.Height);//Средняя яркость 
            ParamAlgorihms.q = ParamAlgorihms.MathExpectation / ParamAlgorihms.Y;
            ParamAlgorihms.Gy = Math.Sqrt(ParamAlgorihms.Y2_sr - Math.Pow(ParamAlgorihms.Y, 2));
            ParamAlgorihms.k = ((1 / ParamAlgorihms.q) * (ParamAlgorihms.Gz / ParamAlgorihms.Gy)) - 1;
            //Цветной алгоритм
            try
            {
                Image.Lock();
                int n = 0;
                for (int row = 0; row < Image.PixelHeight; row++)
                {
                    for (int column = 0; column < Image.PixelWidth; column++)
                    {
                        unsafe
                        {
                            IntPtr pBackBuffer = Image.BackBuffer;
                            double y0 = ParamAlgorihms.RGBColor.B * pixels[n + 2] + ParamAlgorihms.RGBColor.G * pixels[n + 1] + ParamAlgorihms.RGBColor.R * pixels[n]; //Получаем наблюдаемую яркость пикселя
                            if(y0 != 0)
                            {
                                var y = Convert.ToInt32(ParamAlgorihms.q * (y0 + ParamAlgorihms.k * (y0 - ParamAlgorihms.Y)));
                                if (y < 0) y = 0;
                                if (y > 255) y = 255;
                                double deltaY = y - y0;
                                double q = deltaY / y0;
                                double deltaR = q * pixels[n];
                                double deltaG = q * pixels[n + 1];
                                double deltaB = q * pixels[n + 2];
                                byte M = Math.Max(pixels[n], Math.Max(pixels[n + 1], pixels[n + 2]));
                                double deltaM = q * M;

                                if (deltaM + M > 255)
                                {
                                    deltaM = 255 - M;
                                    q = deltaM / M;
                                    deltaR = q * pixels[n];
                                    deltaG = q * pixels[n + 1];
                                    deltaB = q * pixels[n + 2];
                                    //По формуле 4 ищем deltaY
                                    var newDeltaY = ParamAlgorihms.RGBColor.R * deltaB + ParamAlgorihms.RGBColor.G * deltaG + ParamAlgorihms.RGBColor.R * deltaR;
                                    var finalDelta = deltaY - newDeltaY;
                                    var f = finalDelta / (255 - (y0+newDeltaY));
                                    var newDeltaR = f * (255 - (deltaR + pixels[n]));
                                    var newDeltaG = f * (255 - (deltaG + pixels[n + 1]));
                                    var newDeltaB = f * (255 - (deltaB + pixels[n + 2]));

                                    WritePixel(newDeltaR + (deltaR + pixels[n]), newDeltaG + (deltaG + pixels[n + 1]), newDeltaB + (deltaB + pixels[n + 2]));
                                }
                                else
                                {
                                    WritePixel(deltaR + pixels[n], deltaG + pixels[n + 1], deltaB + pixels[n + 2]);
                                }
                            }
                            else
                            {
                                WritePixel(0, 0, 0);
                            }
                            void WritePixel(double R, double G, double B)
                            {
                                
                                //Находим адрес пикселя
                                pBackBuffer += row * source.BackBufferStride;
                                pBackBuffer += column * 4;

                                // Создаем новый цвет
                                int color_data = 255 << 24;
                                color_data |= (int)R << 16; // R
                                color_data |= (int)G << 8;   // G
                                color_data |= (int)B << 0;   // B

                                // Изменяем цвет пикселя
                                *((int*)pBackBuffer) = color_data;
                            }
                        n += 4;
                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show("Ошибка в преобразовании цветного фрагмента");
            }
            finally
            {
                Image.Unlock();
            }
            return Image;
        }
    }
}
