﻿using HighContrastMagnifier.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace HighContrastMagnifier.Services.Interfaces
{
    public delegate void SourceImageEventHandler(WriteableBitmap Source);
    public delegate void FragmentImageEventHandler(WriteableBitmap Fragment);
    public interface IImageProcessingService
    {
        event SourceImageEventHandler ImageSource;
        event FragmentImageEventHandler ImageFragment;
        void OpenImage();
        void SaveImage();
        void FragmentTransformation(AreaImage areaImage);
        void UpdateImage();
    }
}
