﻿using HighContrastMagnifier.Image;
using HighContrastMagnifier.Models;
using HighContrastMagnifier.Services.Interfaces;
using HighContrastMagnifier.Tools;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace HighContrastMagnifier.Services
{
    public class ImageProcessingService : IImageProcessingService
    {
        public event SourceImageEventHandler ImageSource;
        public event FragmentImageEventHandler ImageFragment;
        private BitmapImage Source;
        private WriteableBitmap SourceImage;
        private WriteableBitmap FragmentImage;
        private Strategy sourceStrategy;
        private Strategy fragmentStrategy;

        public ImageProcessingService()
        {
            sourceStrategy = new Strategy(new SourceImage());
            fragmentStrategy = new Strategy(new FragmentImage());
            Settings.StrategySource = sourceStrategy;
            Settings.StrategyFragment = fragmentStrategy;
        }
        public void OpenImage()
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.FileName = "";
            if (openFile.ShowDialog() == true)
            {
                Stopwatch st = new Stopwatch();
                st.Start();
                Uri uri = new Uri(openFile.FileName);
                Source = new BitmapImage(uri);
                SourceImage = new WriteableBitmap(Source);
                sourceStrategy.InitImage(SourceImage);
                sourceStrategy.ProcessImage();
                UpdateImage();
                st.Stop();
                ParamImage.TimeLoadImage= st.Elapsed.ToString();
                ParamImage.HeightImage= Source.PixelHeight.ToString();
                ParamImage.WidthImage = Source.PixelWidth.ToString();
            }
        }
        public void SaveImage()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog(); 
            saveFileDialog.Filter = "Jpeg (*.jpeg)|*.jpeg|Png (*.png)|*.png";
            if (saveFileDialog.ShowDialog() == true)
            {
                switch (saveFileDialog.FilterIndex)
                {
                    case 1:
                        using (FileStream stream = new FileStream(saveFileDialog.FileName, FileMode.Create))
                        {
                            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                            encoder.Frames.Add(BitmapFrame.Create(fragmentStrategy.GetImage()));
                            encoder.Save(stream);
                        }
                        break;
                    case 2:
                        using (FileStream stream = new FileStream(saveFileDialog.FileName, FileMode.Create))
                        {
                            PngBitmapEncoder encoder = new PngBitmapEncoder();
                            encoder.Frames.Add(BitmapFrame.Create(fragmentStrategy.GetImage()));
                            encoder.Save(stream);
                        }
                        break;
                }
            }
        }
        public void FragmentTransformation(AreaImage areaImage)
        {
            Stopwatch st = new Stopwatch();
            st.Start();
            double koefScale = Source.PixelWidth > Source.PixelHeight
                  ? Source.PixelWidth / ParamImage.SizeVirtualFragmentImage.Width
                  : Source.PixelHeight/ ParamImage.SizeVirtualFragmentImage.Height;
            Int32Rect area = new Int32Rect
            {
                Width = (int)(areaImage.Width * koefScale),
                Height = (int)(areaImage.Height * koefScale),
                X = (int)(areaImage.StartPoint.X * koefScale),
                Y = (int)(areaImage.StartPoint.Y * koefScale)
            };

            int stride = Source.PixelWidth * (Source.Format.BitsPerPixel / 8);
            byte[] pixels = new byte[Source.PixelHeight * stride];
            //не копирует если область наизнанку
            Source.CopyPixels(area, pixels, stride, 0);
            FragmentImage = new WriteableBitmap(area.Width, area.Height, 96, 96, PixelFormats.Pbgra32, null);
            FragmentImage.WritePixels(new Int32Rect(0, 0, (int)(areaImage.Width * koefScale), (int)(areaImage.Height * koefScale)), pixels, stride, 0);
            ParamImage.WidthFragment = ((int)(areaImage.Width * koefScale)).ToString();
            ParamImage.HeightFragment = ((int)(areaImage.Height * koefScale)).ToString();
            fragmentStrategy.InitImage(FragmentImage);
            fragmentStrategy.ProcessImage();
            UpdateImage();
            st.Stop();
            ParamImage.TimeProcess = st.Elapsed.ToString();
            }
        public void UpdateImage()
        {
            ImageSource?.Invoke(sourceStrategy.GetImage());
            ImageFragment?.Invoke(fragmentStrategy.GetImage());
        }
    }
}
